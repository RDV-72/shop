$('document').ready(function(){
    loadGoods();
});

function loadGoods() {
    $.getJSON('goods.json', function (data) {
        var out = '';
        for (var key in data){       
            out+='<div class="single-goods">';
            out+='<img src="'+data[key].image+'"alt=" " title="'+data[key].name+'">';
            out+='<h3>'+data[key]['name']+'</h3>';
            out+='<h4>'+data[key]['cost'].toFixed(2)+'</h4>'; 
            out+='<h2>'+(data[key]['cost']*(100-data[key]['discount'])/100).toFixed(2)+'</h2>';                
            out+='<p>-'+data[key]['discount']+'%</p>';
            out+='</div>';
        }
        $('#goods').html(out);
    });
}